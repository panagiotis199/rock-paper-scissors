import {Component, Input} from '@angular/core';
import {Weapon} from '../../models/weapon';

@Component({
    selector: 'app-weapon',
    templateUrl: './weapon.component.html',
    styleUrls: ['./weapon.component.css']
})
export class WeaponComponent {
    @Input() item: Weapon;
    @Input() selected: boolean;
}
