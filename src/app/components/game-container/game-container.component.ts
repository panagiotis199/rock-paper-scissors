import {Component} from '@angular/core';

import {Weapon} from '../../models/weapon';
import {Player} from '../../models/player';

import {Rock, Paper, Scissors} from '../../static/game.weapons';
import {Human, Computer} from '../../static/game.players';

@Component({
    selector: 'app-game-container',
    templateUrl: './game-container.component.html',
    styleUrls: ['./game-container.component.css']
})
export class GameContainerComponent {

    public weapons: Weapon[] = [Rock, Paper, Scissors];
    public Human: Player = Human;
    public Computer: Player = Computer;
    public winner: Player;
    public readyToPlay = true;

    /**
     * @param {Player} player
     */
    setWinner(player: Player): GameContainerComponent {

        this.winner = player;
        this.readyToPlay = false;

        return this;
    }

    /**
     * @return {GameContainerComponent}
     */
    reset(): GameContainerComponent{
        this.Human.selectedWeapon = null;
        this.Computer.selectedWeapon = null;
        this.winner = undefined;
        this.readyToPlay = true;

        return this;
    }
}
