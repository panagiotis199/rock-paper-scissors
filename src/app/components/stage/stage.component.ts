import {Component, EventEmitter, Input, Output} from '@angular/core';

import {Weapon} from '../../models/weapon';
import {Player} from '../../models/player';

import {GameService} from '../../services/game.service';

@Component({
    selector: 'app-stage',
    templateUrl: './stage.component.html',
    styleUrls: ['./stage.component.css']
})
export class StageComponent {

    @Input() weapons: Weapon[] = [];
    @Input() player0: Player;
    @Input() player1: Player;
    @Input() readyToPlay: boolean;

    @Output() winner: EventEmitter<Player> = new EventEmitter();
    @Output() reset: EventEmitter<void> = new EventEmitter();

    constructor(
        private gameService: GameService
    ) {}

    /**
     * Set the user's Instance the selected weapon.
     *
     * @param {Weapon} weapon
     *
     * @return {StageComponent}
     */
    onWeaponSelect(weapon: Weapon): StageComponent {
        if (!this.readyToPlay) {
            return this;
        }

        this.player0.setSelectedWeapon(weapon);

        return this;
    }

    /**
     * Start the game.
     *
     * @return {StageComponent}
     */
    onFightClick(): StageComponent {
        if (!this.player0.selectedWeapon) {
            alert('You must select a weapon first');
            return this;
        }

        this.preparePlayers().getWinner();

        return this;
    }

    /**
     * @return {StageComponent}
     */
    getWinner(): StageComponent {
        this.winner.emit(this.gameService.getWinner(this.player0, this.player1));

        return this;
    }

    /**
     * Reset the winning state and prepare Computer player.
     *
     * @return {StageComponent}
     */
    private preparePlayers(): StageComponent {
        this.player0.unsetWinningState();
        this.player1.unsetWinningState();

        this.prepareComputerPlayer();

        return this;
    }

    private prepareComputerPlayer(): StageComponent {
        const randomWeapon = this.getRandomWeapon();

        this.player1.setSelectedWeapon(randomWeapon);

        return this;
    }

    /**
     * Get a random weapon from those available.
     *
     * @return {Weapon}
     */
    private getRandomWeapon(): Weapon {
        const weaponsAvailable = this.weapons.length;
        const randomIndex = Math.floor(Math.random() * Math.floor(weaponsAvailable));

        return this.weapons[randomIndex];
    }

}
