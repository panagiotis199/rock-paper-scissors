import {Component, Input} from '@angular/core';
import {Player} from '../../models/player';

@Component({
    selector: 'app-game-results',
    templateUrl: './game-results.component.html',
    styleUrls: ['./game-results.component.css']
})
export class GameResultsComponent {
    @Input() player0: Player;
    @Input() player1: Player;
    @Input() winner: Player;
}
