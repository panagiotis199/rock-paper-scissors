import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WeaponComponent } from './components/weapon/weapon.component';
import { StageComponent } from './components/stage/stage.component';
import { GameContainerComponent } from './components/game-container/game-container.component';
import { GameResultsComponent } from './components/game-results/game-results.component';

@NgModule({
  declarations: [
    AppComponent,
    WeaponComponent,
    StageComponent,
    GameContainerComponent,
    GameResultsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
