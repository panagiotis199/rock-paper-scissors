import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {WeaponComponent} from './components/weapon/weapon.component';
import {GameResultsComponent} from './components/game-results/game-results.component';
import {GameContainerComponent} from './components/game-container/game-container.component';
import {StageComponent} from './components/stage/stage.component';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          AppComponent,
          WeaponComponent,
          StageComponent,
          GameContainerComponent,
          GameResultsComponent
      ],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Rock - Paper - Scissors'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Rock - Paper - Scissors');
  }));
});
