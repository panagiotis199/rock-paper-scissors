import {Weapon} from './weapon';

export class Rule {
    weapon: Weapon;
    loosingWeapon: Weapon;
    winningWeapon: Weapon;
}
