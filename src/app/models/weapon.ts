export class Weapon {

    public name: string;

    constructor(name: string) {
        this.name = name;
    }
}
