import {Weapon} from './weapon';

export class Player {

    public readonly type: string;
    public readonly name: string;
    public selectedWeapon: Weapon;
    public isWinner: boolean;

    constructor(name: string, type: string = 'computer') {
        this.name = name;
        this.type = type;
    }

    setSelectedWeapon(weapon: Weapon) {
        this.selectedWeapon = weapon;
    }

    setWinner(): Player {

        this.isWinner = true;

        return this;
    }

    setLooser(): Player {

        this.isWinner = false;

        return this;
    }

    unsetWinningState(): Player {

        this.isWinner = undefined;

        return this;
    }
}
