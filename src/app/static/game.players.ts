import {Player} from '../models/player';

export const Human: Player = new Player('PLAYER0', 'HUMAN');
export const Computer: Player = new Player('PLAYER1', 'COMPUTER');
