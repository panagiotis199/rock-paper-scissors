import {Weapon} from '../models/weapon';

export const Rock: Weapon = new Weapon('ROCK');
export const Paper: Weapon = new Weapon('PAPER');
export const Scissors: Weapon = new Weapon('SCISSORS');

