import {Rule} from '../models/rule';
import {Rock, Paper, Scissors} from './game.weapons';

export const RULES: Rule[] = [
    {weapon: Rock, loosingWeapon: Scissors, winningWeapon: Paper},
    {weapon: Paper, loosingWeapon: Rock, winningWeapon: Scissors},
    {weapon: Scissors, loosingWeapon: Paper, winningWeapon: Rock},
];
