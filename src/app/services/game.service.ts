import {Injectable} from '@angular/core';
import {RuleService} from './rule.service';
import {Player} from '../models/player';

@Injectable({
    providedIn: 'root'
})
export class GameService {

    constructor(
        private ruleService: RuleService
    ) {}

    /**
     * Check between 2 players who wins the round
     * null means it's a tie.
     *
     * @param {Player} human
     * @param {Player} computer
     *
     * @return {Player|null}
     */
    getWinner(human: Player, computer: Player): Player {
        const humanChoice = human.selectedWeapon;
        const computerChoice = computer.selectedWeapon;

        if (humanChoice === computerChoice) {
            return null;
        }

        if (this.ruleService.choice(humanChoice).beats(computerChoice)) {
            return human.setWinner();
        }

        return computer.setWinner();
    }
}
