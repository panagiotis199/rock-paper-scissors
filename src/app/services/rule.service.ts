import {Injectable} from '@angular/core';
import {Rule} from '../models/rule';
import {Weapon} from '../models/weapon';
import {RULES} from '../static/game.rules';

@Injectable({
    providedIn: 'root'
})
export class RuleService {

    public weaponOfChoice: Weapon;

    /**
     * Just a setter for the property "weaponOfChoice"
     *
     * @param {Weapon} weapon
     *
     * @return {RuleService}
     */
    choice(weapon: Weapon): RuleService {
        this.weaponOfChoice = weapon;

        return this;
    }

    /**
     * Check if the give weapon "wins"
     *
     * @param {Weapon} weapon
     *
     * @return {boolean}
     */
    beats(weapon: Weapon): boolean {
        const ruleForSelectedWeapon = this.getRuleFor(this.weaponOfChoice);

        return weapon === ruleForSelectedWeapon.loosingWeapon;
    }

    /**
     * Get a Rule for a given weapon.
     *
     * @param {Weapon} weapon
     *
     * @return {Rule}
     */
    getRuleFor(weapon: Weapon): Rule {
        return RULES.find((rule) => weapon === rule.weapon);
    }

}
